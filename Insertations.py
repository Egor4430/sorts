
mas = [10,20,-10,2,5,8,1,0]

print (mas)

len = len(mas)

for i in range(1, len):
    while i > 0 and mas[i] < mas[i - 1]:
        mas[i], mas[i - 1] = mas[i - 1], mas[i]
        i -= 1

print (mas)